let patrimonio = [];

generateRandomNumbers(10, 1001);

patrimonio.push(randomNumbers);

generateRandomNumbers(10, 1001);

patrimonio.push(randomNumbers);

const saldo = (valores) => {
    let receita = valores[0].reduce((acc, cur) => {
       return acc += cur;
    }, 0);
    
    let despesa = valores[1].reduce((acc, cur) => {
        return acc += cur;
    }, 0);

    if(receita - despesa >= 0) {
        console.log(`O saldo da família é positivo em: ${Math.round((receita - despesa) * 100) / 100}`);
    } else {
        console.log(`O saldo da família é negativo em: ${Math.round((receita - despesa) * 100) / 100}`);
    }
}

saldo(patrimonio);