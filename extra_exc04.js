generateRandomNumbers(5,101,1);

const grades = randomNumbers.map((number) => {
    let grade;
    if(number >= 90) {
        grade = 'A';
    } else if(number >= 80) {
        grade = 'B';
    } else if(number >= 70) {
        grade = 'C';
    } else if(number >= 60) {
        grade = 'D';
    } else {
        grade = 'F';
    }
    return grade;
})

console.log(grades);