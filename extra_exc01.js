generateRandomNumbers(6, 1);

for(x in randomNumbers) {
    randomNumbers[x] = Math.round(randomNumbers[x]);
    if(randomNumbers[x] == 0) {
        randomNumbers[x] = -1;
    } else {
        randomNumbers[x] = 1;
    }
}

console.log(randomNumbers);

const qtd_positivos = randomNumbers.filter((cur) => {
    return cur == 1;
})

console.log(`A quantidade de valores positivos digitados foi: ${qtd_positivos.length}`);