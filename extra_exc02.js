generateRandomNumbers(1, 10, 1);

// Multiplica x por y, z vezes

const recursive_function = (x, y, z) => {   
    while(z > 0) {
        x = x * y;
        z--;
        recursive_function(x, y, z);
    }
    return x;
}

console.log(recursive_function(2, 3, randomNumbers[0]));
