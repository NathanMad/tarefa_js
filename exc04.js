const godsFeatures = gods.map((god) => {return `${god.name}, features: ${god.features.length}`})

const godsMid = gods.filter((god) => {return god.roles.indexOf('Mid') != -1});

const godsPantheonSort = gods.sort((a, b) => {
    if (a.pantheon < b.pantheon) {
        return -1;
    }
    if (a.pantheon > b.pantheon) {
        return 1;
    }
    return 0;
});

const godsRole = gods.map((god) => {return `${god.name} (${god.class})`});

console.log(godsFeatures);

console.log(godsRole);

console.log(godsMid);

console.log(godsPantheonSort);