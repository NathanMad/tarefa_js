const exc01 = function (arr1, arr2) {
    let result = [];

    if (arr1[0].length != arr2.length) {
        alert('Impossível multiplicar os vetores. O número de colunas do primeiro vetor é diferente do número de linhas do segundo');
    } else {
        for (x = 0; x < arr1[0].length; x++) {
            let partialResult = [];
            for (y = 0; y < arr2.length; y++) {
                let summation = 0;
                for (count = 0; count < arr1.length; count++) {
                    summation += arr1[x][count] * arr2[count][y];
                }
                partialResult.push(summation);
            }
            result.push(partialResult);
        }
        console.log(result);
    }
}

let arr1 = [ [4,0], [-1,-1] ];

let arr2 = [ [-1,3], [2,7]]

exc01(arr1, arr2);
