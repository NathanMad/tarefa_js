const booksByCategory = [
    {
        category: "Riqueza",
        books: [
            {
                title: "Os segredos da mente milionária",
                author: "T. Harv Eker",
            },
            {
                title: "O homem mais rico da Babilônia",
                author: "George S. Clason",
            },
            {
                title: "Pai rico, pai pobre",
                author: "Robert T. Kiyosaki e Sharon L. Lechter",
            },
        ],
    },
    {
        category: "Inteligência Emocional",
        books: [
            {
                title: "Você é Insubstituível",
                author: "Augusto Cury",
            },
            {
                title: "Ansiedade – Como enfrentar o mal do século",
                author: "Augusto Cury",
            },
            {
                title: "Os 7 hábitos das pessoas altamente eficazes",
                author: "Stephen R. Covey"
            }
        ]
    }
];

console.log(`categorias: ${booksByCategory.length}`);

const countBooksInCategory = booksByCategory.forEach((item) => {
    console.log(`categoria: ${item['category']} / livros: ${item['books'].length}`)
});

let re = /\s[e]\s/i;

let authors = [];

const countAuthors = booksByCategory.forEach((item) => {
    item['books'].forEach((book) => {
        book['author'].split(re).forEach((author) => {
            if (authors.indexOf(author) == -1) {
                authors.push(author);
            }
        });
    });
});

console.log(authors);
console.log(`número de autores: ${authors.length}`);


const booksByAuthor = (author) => {
    let books = [];
    for (x in booksByCategory) {
        booksByCategory[x]['books'].forEach((book) => {
            if(book['author'].indexOf(author) != -1) {
                books.push(book['title']);
            }
        })
    }
    console.log(`Livros por ${author}: ${books}`);
}

booksByAuthor('Sharon');

